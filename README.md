# centos-bootc

The main branch here is not actively used. The currently maintained
branches are:

- [c10s](https://gitlab.com/redhat/centos-stream/containers/bootc/-/tree/c10s?ref_type=heads)
- [c9s](https://gitlab.com/redhat/centos-stream/containers/bootc/-/tree/c9s?ref_type=heads)
